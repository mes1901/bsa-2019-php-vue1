/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';

window.Vue.use(VueRouter);

import UsersIndex from './components/UsersIndex.vue';
import UsersCreate from './components/UsersCreate.vue';
import UsersEdit from './components/UsersEdit.vue';

const routes = [
    {
        path: '/',
        components: {
            usersIndex: UsersIndex
        }
    },
    {path: '/users/create', component: UsersCreate, name: 'createUser'},
    {path: '/users/edit/:id', component: UsersEdit, name: 'editUser'},
]

const router = new VueRouter({ routes });

const app = new Vue({ router }).$mount('#app');

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));



