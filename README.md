О приложении

1 - Создание списка пользователей

2 - Добавление и удаление пользователя

3 - Редактирование пользователя

4 - Поиск пользователя

Установка

Для корректной установки на вашем сервере должны быть установлены PHP 7.1 и Composer. 

Для установки сделайте следующее:

1 - Склонировать проект на локальную машину:
git clone https://mes1901@bitbucket.org/mes1901/bsa-2019-php-vue1.git

2 - Перейдите в папку приложения с помощью команды cd
Запустите composer install

3 - Установить все зависимости приложения через Composer
composer install

4 - Создать базу данных приложения, выполнив SQL-запрос в MySQL CREATE DATABASE

5 - Скопируйте файл .env.example в .env в корневой папке. Откройте файл .env и измените имя базы данных (DB_DATABASE) на то, что у вас есть, DB_USERNAME имени пользователя (DB_USERNAME) и пароля (DB_PASSWORD) соответствуют вашей конфигурации.

6 - Запустить скрипт генерации таблиц БД php artisan key:generate

7 - Выполнить миграцию php artisan migrate

8 - Заполнить базу данными (не обязательно) php artisan db:seed

9 - Запустить веб-сервер php artisan serve

10 - Открыть страницу в браузере: http://localhost:8000/